import builtins
import discord
import datetime
import asyncio
import configparser
import random
import re
import os
import urllib3
from bs4 import BeautifulSoup
import mysql.connector

def print(s):
  now = datetime.datetime.now()
  builtins.print(now.strftime(f"[%d/%m/%Y, %H:%M:%S] {s}"))

def connectDB():
  mydb = mysql.connector.connect(
    host="localhost",
    user="kinsteen",
    password=db_pass,
    database="khezzoum"
  )

  mycursor = mydb.cursor()
  return (mydb, mycursor)

def getProverbesUrl():
  http = urllib3.PoolManager()
  r = http.request("POST", "http://lesbeauxproverbes.com/wp-admin/admin-ajax.php", fields={"action":"go_random"})
  url = r.data.decode("utf-8")
  r2 = http.request("GET", url)
  content = r2.data.decode("utf-8")
  soup = BeautifulSoup(content, 'html.parser')
  img = soup.select('.entry-header .featimg img')[0]
  print("fetched proverbes.")
  return img["src"]

def geturl(url):
    http = urllib3.PoolManager()
    response = http.request("GET", url)
    if len(response.retries.history):
        return response.retries.history[-1].redirect_location
    else:
        return url

def intersect(lst1, lst2):
  lst3 = [value for value in lst1 if value in lst2]
  return len(lst3) > 0

async def check_je_suis(message):
  triggers = ["je suis", "chui", "chuis", "jui", "jsuis", "j'suis", "jsui", "j'sui"]
  for trig in triggers:
    idx = message.content.lower().find(trig + " ")
    if idx != -1:
      formatted = message.content[idx+len(trig)+1::].strip()
      await message.channel.send("Salut " + formatted + ", moi c'est Khezzoum.")

async def remove_mute(member, guild, time):
  await asyncio.sleep(time)
  await member.remove_roles(discord.utils.get(guild.roles, name="Muted"))
  print("Removed mute of " + member.name + ", was up for " + str(time) + "s")

class MyClient(discord.Client):
  async def on_ready(self):
    print('Logged on as {0}!'.format(self.user))
    self.reg = r"^((((<:\w+:\d+>)|\\\\(u\w{4}|U\w{8})| )+)|(:[a-zA-Z]{,13}:))$" # Regex for the emote-only channel
    self.run = False
    self.emotes = ["ayaya", "toutou", "ping", "jaune", "rouge", "uwu", "jetonds", "canard"]
    self.overwrite_name = False
    self.force_update = False
    self.edited = []
    for guild in self.guilds:
      muted_role = discord.utils.get(guild.roles, name="Muted")
      for muted_member in muted_role.members:
        await muted_member.remove_roles(muted_role)
      
      vives_member = discord.utils.get(guild.members, id=213468631013654528)
      if vives_member != None:
        asyncio.create_task(self.rename_vives(vives_member, 0)) # Launch in "another thread", to not be blocking
        print(vives_member.activity)

  async def on_member_join(self, member):
    if member.id == 213468631013654528 and member.guild.id == 488440194421358623:
      await member.add_roles(discord.utils.get(member.guild.roles, name="Olivié"))
      await member.edit(nick="100% il a esseyé")
  
  async def on_member_update(self, before, after):
    member = after
    #print("Update")
    if member.id == 213468631013654528: # my id for now
      lol_present = False
      for act in member.activities:
        if act.name == "League of Legends":
          lol_present = True
          if not vives_playing:
            # print("Vives is playing lol")
            # check if ranked game with desc
            # query riot api to get champ name
            # get grade champ_grade[champ_name]['grade']
            # get comment champ_grade[champ_name]['grade']
            # send message
            vives_playing = True
      #if not lol_present:
        #print("Vives is not playing anymore")
        # send message?


  async def on_message(self, message):
    if message.author == client.user: # Do not loop on self message
      return

    sec = random.randint(5,60)
    chances = 500
    resp = f"MENFOU, t'as {sec}s pour comprendre à quel point on en a rien à foutre."

    if message.channel.id != 695317952479756399: # travail channel id
      if random.randint(1,chances) == 1:
        if random.randint(1,100) > 80:
          await message.reply("WTF ?!")
        else:
          await message.reply(resp)
          muted = discord.utils.get(message.guild.roles, name="Muted")
          await message.author.add_roles(muted)
          asyncio.create_task(remove_mute(message.author, message.guild, sec)) # This will "fire and forget", so it will continue but run in background

    if "preuve" in message.content.lower():
      await message.channel.send("Exercice.")
    if "manuellement" in message.content.lower():
      await message.channel.send("Automatiquement.")
    if "source" in message.content.lower() and "source=" not in message.content.lower():
      await message.channel.send(random.choice(["Tkt.", "LesBeauxProverbes.com", "PasseportSanté.com", "WikiHow.com", "Doctissimo.fr", "Regarde là : " + geturl("https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard"), "Ici : || https://nhentai.net" + geturl("https://nhentai.net/random/") + ' ||']))
    if "probablement" in message.content.lower():
      await message.channel.send("PDG du bâtiment.")
    if intersect(["hotel", "hôtel"], message.content.lower()):
      await message.channel.send("Trivago.")
    if intersect(["négocie", "negocie"], message.content.lower()):
      await message.channel.send("On ne négocie pas avec les terroristes.")
    if intersect(["j'ai fai", "jai fai", "g fai", "g fé"], message.content.lower()):
      await message.channel.send("Tu n'as rien fait.")
    if "sah" in message.content.lower() and "plaisir" not in message.content.lower():
      await message.channel.send("Quel PLAISIR.")

    await check_je_suis(message)

    if message.channel.id == 700102076205629461: # emote-only channel id
      if re.search(self.reg, str(message.content.encode("unicode-escape"))[2:-1]) == None:
        await message.delete()
    
    #-- COMMANDES --#
    if message.content.startswith("!banvives"):
      newPseudo = message.content[10:]
      if (len(message.content[10:]) >= 32):
        await message.channel.send(f"Il faut donner un pseudo avec 32 caractères ou moins, ici y en a {len(newPseudo)}.")
      else:
        m = await message.channel.send(f"Vous voulez punir Vives en le renommant \"{newPseudo}\" ?")
        await m.add_reaction('👍')
        await m.add_reaction('👎')
        asyncio.create_task(self.check_punish(m, m.channel, 1, message.content[10:]))
    elif message.content.startswith("!say"):
      await message.delete()
      await message.channel.send(message.content[5:])
    elif message.content.startswith("!check"):
      if len(message.content.split(" ")) == 2:
        emote = message.content.split(" ")[1]
        if os.path.isfile(emote + "s_" + str(message.guild.id) + ".txt"):
          f = open(emote + "s_" + str(message.guild.id) + ".txt", "r+")
          data = f.read().splitlines()
          result = ""
          for e in message.guild.emojis:
            if emote in str(e):
              result += "Voici les résultats pour l'emote " + str(e) + " :\n"
          for idx,line in enumerate(data):
            userid = int(line.split(":")[0])
            nb = line.split(":")[1]
            if self.get_user(userid) != None:
              if int(nb) > 1:
                result += str(idx+1) + ": " + (self.get_user(userid).name + " avec " + nb + " " + emote + "s.\n")
              else:
                result += str(idx+1) + ": " + (self.get_user(userid).name + " avec " + nb + " " + emote + ".\n")
            else:
                result += str(idx+1) + ": quelqu'un qui s'est fait ban avec " + nb + " " + emote + "s.\n"
            self.run = False
          await message.channel.send(result)
          f.close()
        else:
          await message.channel.send("Je ne compte pas cette emote.")
      else:
        await message.channel.send("Il me faut le nom de l'emote.")
    elif message.content.startswith("!update"):
      if message.author.id == 95468162144808960:
        await self.update_emote_counts(message.content.split(' ')[1], message)
      else:
        await message.channel.send("Vous n'avez pas les droits de lancer cette commande.")
    elif message.content.startswith("!proverbe"):
      imgurl = getProverbesUrl()
      await message.channel.send(imgurl)
    elif message.content.lower().startswith("!clem"):
      print("DB reconnecting")
      (mydb, mycursor) = connectDB()
        
      ret_arr = message.content.split(' ')
      if len(ret_arr) == 1:
        m = f"Résumé du retard de Clément :\n"
        mycursor.execute("SELECT SUM(retard) FROM retard")
        myresult = mycursor.fetchall()[0]
        m += f"Total : {myresult[0]} minutes.\n"

        mycursor.execute(f"SELECT SUM(retard) FROM retard WHERE date > '2022-{datetime.datetime.now().month}-01'")
        myresult = mycursor.fetchall()[0]
        m += f"Ce mois-ci : {myresult[0]} minutes."
        await message.channel.send(m)
      else:
        ret = ret_arr[1]
        if ret.isdigit():
          ret = int(ret)
          print(f"Clem is {ret}m late")
          await message.channel.send(f"Clément vient d'arriver avec {ret} minutes de retard...")
          sql = "INSERT INTO retard (retard) VALUES (%s)"
          val = [ret]
          mycursor.execute(sql, val)
          mydb.commit()
        else:
          await message.channel.send(f"Il faut mettre un entier pour les minutes.")

  async def on_message_edit(self, before, after):
    if before.author.id == 192189611123015680 and before.id not in self.edited:
      await before.channel.send("Clément vient d'éditer son message qui disait : " + before.content)
      self.edited.append(before.id)

  async def on_raw_message_edit(self, payload):
    channel = self.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    if message.channel.id == 700102076205629461 and re.search(self.reg, str(message.content.encode("unicode-escape"))[2:-1]) == None:
        await message.delete()

  async def on_raw_reaction_add(self, payload):
    channel = self.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    emoji = payload.emoji
    if intersect(self.emotes, emoji.name):
      dic = dict()
      with open(emoji.name + "s_" + str(message.guild.id) + ".txt", "r+") as f:
        data = f.read().splitlines()
        for l in data:
          dic[l.split(":")[0]] = int(l.split(":")[1])
      
      if str(message.author.id) in dic:
        dic[str(message.author.id)] = int(dic[str(message.author.id)]) + 1
      else:
        dic[str(message.author.id)] = 1

      with open(emoji.name + "s_" + str(message.guild.id) + ".txt", "w+") as f:
        result = ""
        for u in sorted(dic, key=dic.get, reverse=True):
          result += u + ":" + str(dic[u]) + "\n"
        f.write(result)

  async def on_raw_reaction_remove(self, payload):
    channel = self.get_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    emoji = payload.emoji
    if intersect(self.emotes, emoji.name):
      dic = dict()
      with open(emoji.name + "s_" + str(message.guild.id) + ".txt", "r+") as f:
        data = f.read().splitlines()
        for l in data:
          dic[l.split(":")[0]] = int(l.split(":")[1])

      if int(dic[str(message.author.id)]) == 1:
        dic.pop(str(message.author.id), None)
      else:
        dic[str(message.author.id)] = int(dic[str(message.author.id)]) - 1

      with open(emoji.name + "s_" + str(message.guild.id) + ".txt", "w+") as f:
        result = ""
        for u in sorted(dic, key=dic.get, reverse=True):
          result += u + ":" + str(dic[u]) + "\n"
        f.write(result)

  async def on_message_delete(self, message):
    if message.author.id == 192189611123015680:
      await message.channel.send("Clément vient de supprimer son message qui disait : " + message.content)

  async def remove_force(self):
    await asyncio.sleep(43200)
    self.overwrite_name = False
    self.force_update = True # Force update even if delay isn't passed
    print("Force removed")

  async def rename_vives(self, member, delay):
    gdelay = 1
    await asyncio.sleep(gdelay + delay)
    if self.overwrite_name == False:
      if delay <= 0 or self.force_update:
        self.force_update = False
        pseudos = []
        with open("pseudo_vives.txt", "r+") as f:
          data = f.read().splitlines()
          for l in data:
            pseudos.append(l)

        r_idx = random.randint(0, len(pseudos)-1)
        print("Chose " + pseudos[r_idx])
        await member.edit(nick=pseudos[r_idx])

        r_delay = random.randint(5,21600)
        print(f"Delay = {round(r_delay/3600-((r_delay/3600)%1))}h{round((r_delay%3600)/60)}m{round((r_delay%60))}s")
        asyncio.create_task(self.rename_vives(member, r_delay - gdelay))
      else:
        asyncio.create_task(self.rename_vives(member, delay - gdelay))
    else:
      asyncio.create_task(self.rename_vives(member, delay - gdelay))

  async def check_punish(self, message, channel, callcount, pseudo):
    await asyncio.sleep(2)
    c = True
    message = await channel.fetch_message(message.id)
    up = 0
    down = 0
    for reac in message.reactions:
      if reac.emoji == '👍':
        up = reac.count
      elif reac.emoji == '👎':
        down = reac.count
    if up >= 10 or down >= 10 or callcount == 20:
      c = False
      await channel.send("Le peuple a décidé.")
      if up > down:
        if len(pseudo) == 0:
          pseudo = "Le Connard de Service"
        await channel.send(f"Vives sera donc nommé \"{pseudo}\" pour les prochaines 12h.")
        vivesmember = discord.utils.get(message.guild.members, id=213468631013654528)
        await vivesmember.edit(nick=pseudo)
        self.overwrite_name = True
        self.force_update = False
        asyncio.create_task(self.remove_force())
      else:
        await channel.send("Vives continuera de vivre avec son pseudo actuel.")
        self.overwrite_name = False

    if c == True:
      asyncio.create_task(self.check_punish(message, channel, callcount+1, pseudo))

  async def update_emote_counts(self, emote, message):
    if self.run == False:
      await message.channel.send("Je calcule... Depuis la création du serveur. Ça prendra genre 4 heures.")
      with message.channel.typing():
        before = datetime.datetime.now()
        self.run = True
        dic = dict()
        for chan in message.guild.channels:
          if isinstance(chan, discord.TextChannel):
            print("Doing channel " + chan.name + "...")
            async for m in chan.history(limit=None, after=datetime.datetime(2019,10,15)):
              for reac in m.reactions:
                if emote in str(reac):
                  auth = m.author.id
                  if auth in dic:
                    dic[auth] += reac.count
                  else:
                    dic[auth] = reac.count
        print("Done.")
        f = open(emote + "s_" + str(message.guild.id) + ".txt.new", "w+")
        data = ""
        for u in sorted(dic, key=dic.get, reverse=True):
          if self.get_user(u) != None:
            data += str(self.get_user(u).id) + ":" + str(dic[u]) + "\n"
        self.run = False
        f.write(data)
        delta = datetime.datetime.now()-before
      await message.channel.send("Fini en " + str(delta.seconds/60) + " minutes. Vérifie les comptes.")
      f.close()
    else:
      await message.channel.send("Déjà en train de chercher !")

config = configparser.ConfigParser()
config.read('config.ini')
token = config.get('Bot', 'Token')

db_pass = config.get('Mysql', 'Password')

intents = discord.Intents.default()
intents.members = True
intents.presences = True
client = MyClient(intents=intents)
client.run(token)
